const router = require('express').Router();
const { response } = require('express');
const { google } = require('googleapis');

const   GOOGLE_CLIENT_ID = '748135360872-ji0seb6it4o949jo038nhbtmb2ek4mi1.apps.googleusercontent.com'

const REFRESH_TOKEN = 'ya29.A0ARrdaM-w1Jif_pImk3_mxUTucxv5mJiiCJ7-B7twB9w4DxpH8It9fUgiHFhHZXfeuwT8gtUO_Z1mM3ttX6bNG44ADMWH8Lt96vDexB7BRacRfxJe2XkC1KlbGLWh1t7WMRc2ZHeh_mlZAXQ53HXgR2_-3L1f'

const oauth2Client = new google.auth.OAuth2(
  GOOGLE_CLIENT_ID,
  'http://localhost:3000'
)

router.get('/', async (req, res, next) => {
  res.send({ message: 'Ok api is working 🚀' });
});


  router.post('/create-tokens', async (req, res, next) => {
    try {
      const { code } = req.body
      const {tokens} = await oauth2Client.getToken(code)
      res.send(tokens)
      res.send(code)
    }catch (error){
      next(error)
    }
  })

  router.post('/create-event', async(req, res, next) => {
    try{
      const {description, startDateTime, endDateTime} = req.body
      oauth2Client.setCredentials({refresh_token: REFRESH_TOKEN})
      const calendar = google.calendar('v3')
      const response = await calendar.events.insert({
        auth:oauth2Client,
        calendarId: 'primary',
        requestBody: {
          description: description,
          start: {
            dateTime: new Date(startDateTime),
          },
          end: {
            dateTime: new Date(endDateTime),
          }
        }
      })
      res.send(response)
    }catch(error) {
      next(error)
    }
  })

module.exports = router;
