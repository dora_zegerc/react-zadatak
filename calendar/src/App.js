import React, { useState } from "react";
import {GoogleLogin} from "react-google-login";
import { Calendar } from "react-bootstrap-icons";
import axios from "axios";
import { useState } from 'react';
function App() {
    const successLoginResponse = (response) => {
        console.log(response)
        const { code } = response
        axios.post('http://localhost:4000//api/create-tokens', {code})
        .then(response => {
          console.log(response.data)
        })
        .catch(error => console.log(error.message))
    }
    const failureLoginResponse = (error) => {
        console.log(error)
    }

    const handleSubmit = (e) => {
      e.preventDefault(description, startDateTime, endDateTime)
      axios.post('/api/create-event', {
        description, startDateTime, endDateTime
      })
      .then(response => {
        console.log(response.data)
      })
      .catch(error => console.log(error.message))
    }

      const [description, setDescription] = useState('')
      const [startDateTime, setStartDateTime] = useState('')
      const [endDateTime, setEndDateTime] = useState('')

    return (
      <>  <div>
            <div className="wrap">
                <Calendar height={50} width={50}/>
                <h1>Google Calendar App</h1>
                <div>
                <GoogleLogin
                    clientId="748135360872-ji0seb6it4o949jo038nhbtmb2ek4mi1.apps.googleusercontent.com"
                    buttonText="Log in with Google"
                    onSuccess={successLoginResponse}
                    onFailure={failureLoginResponse}
                    cookiePolicy={'single_host_origin'}
                    scope="https://www.googleapis.com/auth/calendar"
                />
                </div>
            </div>
        </div>
        <div>
          <form onSubmit={handleSubmit}>
              <label htmlFor="description">Description</label>
              <br/>
              <input type='text'
                     id='description' 
                     value={description} 
                     onChange={e => setDescription(e.target.value)}/>
              <br/>
              <label htmlFor="startDateTime">Date start</label>
              <br/>
              <input type='datetime-local'
                     id='startDateTime' 
                     value={startDateTime} 
                     onChange={e => setStartDateTime(e.target.value)}/>
              <br/>
              <label htmlFor="endDateTime">Date end</label>
              <br/>
              <input type='datetime-local'
                     id='endDateTime' 
                     value={endDateTime} 
                     onChange={e => setEndDateTime(e.target.value)}/>
              <br/>
              <button type='submit'>Add event</button>
          </form>
        </div>
       </> 
    );
}
export default App;
