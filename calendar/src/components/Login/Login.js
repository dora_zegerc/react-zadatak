import React from "react";
import {GoogleLogin} from "react-google-login";
import { Calendar } from "react-bootstrap-icons";

function Login() {
    const successLoginResponse = (response) => {
        console.log(response)
    }
    const failureLoginResponse = (error) => {
        console.log(error)
    }

    return (
      <>  <div>
            <div className="wrap">
                <Calendar height={50} width={50}/>
                <h1>Google Calendar App</h1>
                <div>
                <GoogleLogin
                    clientId="748135360872-ji0seb6it4o949jo038nhbtmb2ek4mi1.apps.googleusercontent.com"
                    buttonText="Log in with Google"
                    onSuccess={successLoginResponse}
                    onFailure={failureLoginResponse}
                    cookiePolicy={'single_host_origin'}
                    scope="https://www.googleapis.com/auth/calendar"
                />
                </div>
            </div>
        </div>
       </> 
    );
}
export default Login;

